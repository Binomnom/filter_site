//выгрузка html-кода страницы "<body>...</body>"
const html = document.documentElement.outerHTML

//парсинг на наличие комментариев
let commentArr = html.match(/<!--.*?-->/g)
// парсинг на наличие email адресов
let emailArr = html.match(/[\w]+@[\w-]+\.[\w.]{2,}/gi)
//парсинг на наличие телефонных номеров
let numberArr = html.match(/(\+7|8)(\s)?\([0-9]{3}\)(\s)?[0-9]{3}(-)?[0-9]{2}(-)?[0-9]{2}/g)
//парсинг на наличие ссылок на сторонние ресурсы
let urlArr = html.match(/(http|https|ftp):\/\/[\w.\/-]+/g)

//парсинг в коде переменных по ключевым словам  
let passRex = html.match(/(password|login|root|admin)[\s-=_]+([\d_\w\s-]+)/gi)

//парсинг на наличие в коде функций, работающий с БД
let dbFuncArr = html.match(/\b[\w._]*(connect|sql|pdo|connection|exec)\w*[\.\s]?\([\w,.\s$"'*>]*\)/gmi)

//обработчик поступающих сообщений в content.js
browser.runtime.onMessage.addListener(
    function (request, sender, sendResponse){
        if(request.name === 'scan'){
            sendResponse({
                //отправка результатов поиска регулярками с удалением повторений
                comments: commentArr === null ? 'Not found' : [...new Set(commentArr)].join('\n'),
                emails: emailArr === null ? 'Not found' : [...new Set(emailArr)].join('\n'),
                numbers: numberArr === null ? 'Not found' : [...new Set(numberArr)].join('\n'),
                urls: urlArr === null ? 'Not found' : [...new Set(urlArr)].join('\n'),
                passwords: passRex === null ? 'Not found' : [...new Set(passRex)].join('\n'),
                currentSite: document.location.origin,
                dbFunc: dbFuncArr === null ? 'Not found' : [...new Set(dbFuncArr)].join('\n')
            })
        }
        else if(request.name === 'crowling'){
            let arr = crowling(document.location.href)
            sendResponse({
                responseArr: [...new Set(arr)]
            })
        }
    }
)

//функция для поиска относительных ссылок с текущей страницы
function crowling(href){
    //удаление лишнего знака "/" в конце пути 
    let link = href
    if(link.toString().slice(-1) == '/'){
        link = link.toString().slice(0,link.length - 1)
    }

    // поиск в html ссылок для перехода с текущей страницы  
    let arr = html.matchAll(/href="(\/[\w\?=!]+[\w\/\?=!]*)"/gmi)
    arr = Array.from(arr)

    // массив ссылок для запросов
    let linksArr = new Array()
    if(arr.length > 0){
        for(let i = 0; i < arr.length; i++){
            //склейка адреса текущей страницы и относительных ссылок
            linksArr.push(link + arr[i][1])
        }
    }

    return linksArr
}