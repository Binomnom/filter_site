var sectionInform = document.getElementById('parseInformation')

//нахожение кнопки создания файла
var crowler_create = document.getElementById('crowler-create')

//нахожение кнопки скачивания файла
var crowler_download = document.getElementById('crowler-download')

//текст о начале формирования файла
var span = document.getElementById('text-about-create-file')
 
//описание о корректной работе расширения
let textAboutLoadingPage = document.getElementById('text-about-loading-page')

//кнопка для начала сканирования страницы
let scanButton = document.getElementById('scan')

//отправка сообщения в content.js при нажатии на кнопку
scanButton.addEventListener('click', async (e) => {

    //определение активной вкладки
    let queryOptions = { active: true, currentWindow: true}
    let tab = await browser.tabs.query(queryOptions)

    //сообщение-сигнал для content.js на сканирование страницы
    let message = {name: 'scan'}

    scanButton.setAttribute('hidden', 'true')
    //обработка полученного ответа от content.js
    browser.tabs.sendMessage(tab[0].id, message, function(response) {
        //вывод полученных данных от content.js
        blockMolding('Comments', response.comments)
        blockMolding('Email', response.emails)
        blockMolding('Numbers', response.numbers)
        blockMolding('URLS', response.urls)
        blockMolding('Keywords - {login,admin,root,password}', response.passwords)
        blockMolding('Function work with database', response.dbFunc)
        addRobotsTxt(response.currentSite)
        
        
        //скрытие информации при поступлении основной информации
        textAboutLoadingPage.setAttribute('hidden', 'true')
        crowler_create.toggleAttribute('hidden')
    })
}
)

let reqResult = []

crowler_create.addEventListener('click', async() => {
    //скрытие кнопки создания
    crowler_create.hidden = true

    //отображение текста о начале формирования файла
    span.hidden = false

    //определение активной вкладки
    let queryOptions = { active: true, currentWindow: true}
    let tab = await browser.tabs.query(queryOptions)

    //сообщение-сигнал для content.js для "опроса" обнаруженных ссылок
    let message = {name: 'crowling'}

    //отправка сообщения 
    browser.tabs.sendMessage(tab[0].id, message, function(response) {
        reqResult = response.responseArr
        
        //вызов обработчика для скачивания файла
        crowler_download.click()
    })
})

crowler_download.addEventListener('click', () => {
    //скачивание файла
    downloadAsFile(reqResult)
})

function addRobotsTxt(link){
    fetch(link + "/robots.txt")
            .then(response => response.text())
            .then(data => blockMolding('robots.txt', data))
            .catch(error => blockMolding('robots.txt', error))
}

//функция формирования найденных значений с помощью content.js в html коде 
function blockMolding(_headerValue, _arrValues){

    //контейнер
    let div = document.createElement('div')
    div.className = 'divTexts'

    //элемент для заголовка текста
    let header = document.createElement('p')
    header.className = 'headerTexts'
    header.innerText = _headerValue + ':'

    //элемент для вывода текста
    let span = document.createElement('span')
    span.className = 'spanTexts'

    //добавление найденных значений в span
    span.innerText = _arrValues

    //отображение созданного контейнера 
    sectionInform.appendChild(div)
    div.appendChild(header)
    div.appendChild(span)
}

//функция скачивания файла
async function downloadAsFile(data){
    data = await requests(data)
    let a = document.createElement('a')
    let file = new Blob([data.join('\n')], {type: 'application/json'})
    a.href = URL.createObjectURL(file)
    a.download = 'crowling.txt'

    //скрытие текста
    span.hidden = true

    //вывод кнопки с предложением формирования файла
    crowler_create.hidden = false

    a.click()
}

//формирование списка, по правилу ключ = ссылка, значение = код ответ
async function requests(links){
    let tmp = []
    for(let i = 0; i < links.length; i++){

        //очередной запрос и код ответа
        let req = fetch(links[i])
        let statusCode = (await req).status

        //занесение результата в список
        tmp.push(`Link: ${links[i]}, Code: ${statusCode}`)
    }

    return tmp
}